import React, { useState } from "react";
import { useEffect } from "react";
import { getUserRepositories } from "../services/api";
import LinkTiles from "./LinkTiles";

const RepositoriesByUser: React.FC = (props: { name: string }) => {
  const [repositories, setRepositories] = useState([]);

  useEffect(() => {
    const getRepositories: any = async () => {
      const response: [] = await getUserRepositories(props.name);

      setRepositories(response);
    };

    getRepositories();
  });

  return (
    <div className="grid md:grid-cols-4 gap-5 md:gap-7 place-items-stretch">
      {repositories.map((repository) => (
        <a href={repository.html_url} className="" >
          <LinkTiles
            textTitle={repository.name}
            description={repository.description}
          />
        </a>
      ))}
    </div>
  );
};

export default RepositoriesByUser;
