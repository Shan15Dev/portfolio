import {Component, ReactNode, useState} from 'react'
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Nav from "./components/Nav";
import Home from "./pages/Home";
import Projects from "./pages/Projects";
import About from "./pages/About";


function App() {
    return (
        <Router>
            <div className="dark:bg-slate-800" id="root">
                <Nav/>
                <Routes>
                    <Route path="/" element={<Home />}></Route>
                    <Route path="/projects" element={<Projects />}></Route>
                    <Route path="/about" element={<About />}></Route>
                </Routes>
            </div>
        </Router>

    )
}

export default App
