import React from "react";
import { Link } from "react-router-dom";

const LinkTiles = (props: {
  path: string;
  textTitle: string;
  description: string;
}) => {
  return props.path ? (
    <Link
      className="block cursor-pointer md:col-start-3 md:col-span-2 p-10 rounded-lg shadow-lg bg-slate-700 max-w-sm md:hover:scale-110 transition duration-500"
      to={`${props.path}`}
    >
      <h1 className="text-white text-xl leading-tight font-medium mb-2">
        {props.textTitle}{" "}
      </h1>
      <p className="text-white text-md leading-tight font-medium mb-2">
        {props.description}
      </p>
    </Link>
  ) : (
    <div className="block cursor-pointer md:col-start-3 md:col-span-2 p-10 rounded-lg shadow-lg bg-slate-700 max-w-sm md:hover:scale-110 transition duration-500">
      <h1 className="text-white text-xl leading-tight font-medium mb-2">
        {props.textTitle}{" "}
      </h1>
      <p className="text-white text-md leading-tight font-medium mb-2">
        {props.description}
      </p>
    </div>
  );
};

export default LinkTiles;
